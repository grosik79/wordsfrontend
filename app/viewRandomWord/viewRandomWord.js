'use strict';

angular.module('myApp.viewRandomWord', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/viewrandomword', {
            templateUrl: 'viewRandomWord/viewRandomWord.html',
            controller: 'viewRandomWordCtrl'
        });
    }])


    .controller('viewRandomWordCtrl', ['$http', function ($http) {
        var URL = 'http://localhost:8080';
        var self = this;
        this.words = [];
        this.languages = [];
        //
        // this.wordtranslation = {
        //     'translation': '',
        //     'idWord': '',
        //     'languageId': ''
        // }

        this.sendToBackend = function () {
            $http.get(URL + "/word/random")
                .then(function (data) {
                    console.log(data);
                    document.getElementById("randomWord").value = data.data.original;
                }, function (data) {
                    console.log(data);
                    console.log(self.Word);
                });
        }
        this.fetchLanguages = function () {
            $http.get(URL + "/lang/list").then(
                function (response) {
                    console.log(response);
                    self.languages = [];
                    for (var index in response.data){
                        self.languages.push(response.data[index]);
                    }
                },
                function (resp) {
                    console.log("Error: " + resp)
                }
            );
        };

        this.fetchLanguages();

    }]);





