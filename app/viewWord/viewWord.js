'use strict';

angular.module('myApp.viewWord', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/viewword', {
            templateUrl: 'viewWord/viewWord.html',
            controller: 'ViewWordCtrl'
        });
    }])

    .controller('ViewWordCtrl', ['$http', function ($http) {
        var URL = 'http://localhost:8080';
        var self = this;
        this.words = [];
        this.languages = [];

        this.wordtranslation = {
            'translation': '',
            'idWord': '',
            'languageId': ''
        };
        this.sendToBackend = function () {
            $http.post(URL + "/trans/addTrans", self.wordtranslation)
                .then(function (data) {
                    console.log(data);
                }, function (data) {
                    console.log(data);
                    console.log(self.Word);
                });

    };

        this.fetchWords = function () {
            $http.post(URL + "/word/list")
                .then(function (response) {
                        console.log(response);
                        self.word = [];
                        for (var index in response.data) {
                            self.word.push(response.data[index]);
                        }
                    },
                    function (resp) {
                        console.log("Error: " + resp)
                    }
                );
        };
        this.fetchWords();



        this.fetchLanguages = function () {
            $http.get(URL + "/lang/list").then(
                function (response) {
                    console.log(response);
                    self.languages = [];
                    for (var index in response.data){
                        self.languages.push(response.data[index]);
                    }
                },
                function (resp) {
                    console.log("Error: " + resp)
                }
            );
        };

        this.fetchLanguages();


    }]);